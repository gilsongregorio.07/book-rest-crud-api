create database restful;
create table restful.books
(
    id          int auto_increment
        primary key,
    isbn        varchar(255)   not null,
    name        varchar(255)   not null,
    genre       varchar(255)   not null,
    authorName varchar(255)   not null,
    price       decimal(15, 2) not null
);

