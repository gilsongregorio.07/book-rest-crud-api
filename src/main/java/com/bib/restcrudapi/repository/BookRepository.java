package com.bib.restcrudapi.repository;

import com.bib.restcrudapi.model.Book;
import org.apache.ibatis.annotations.*;

import java.util.List;


@Mapper
public interface BookRepository {

    @Select("select * from books")
    List<Book> findAll();

    @Select("SELECT * FROM books WHERE id = #{id}")
    Book findById(long id);

    @Select("SELECT * FROM books WHERE isbn = #{isbn} AND id!=#{id}")
    Book findByIsbn(String isbn, Long id);

    @Delete("DELETE FROM books WHERE id = #{id}")
    int deleteById(long id);

    @Insert("INSERT INTO books(isbn, name, genre, authorName, price) " +
            "VALUES (#{isbn},#{name}, #{genre}, #{authorName}, #{price})")
    @Options(useGeneratedKeys=true, keyProperty="id")
    int insert(Book book);

    @Update("Update books set isbn=#{isbn}, name=#{name}, " +
            "genre=#{genre}, authorName=#{authorName}, price=#{price}  where id=#{id}")
    int update(Book book);

    @Update("Update books set name=#{newName} where id=#{id}")
    int updateName(long id, String newName);
}



