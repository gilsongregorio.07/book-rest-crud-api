package com.bib.restcrudapi;

import ch.vorburger.exec.ManagedProcessException;
import ch.vorburger.mariadb4j.MariaDB4jService;
import ch.vorburger.mariadb4j.springframework.MariaDB4jSpringService;
import org.springframework.boot.Banner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
@SpringBootApplication(scanBasePackages = "com.bib")
public class RestMainApp implements ExitCodeGenerator {

    private final MariaDB4jSpringService mariaDB4j;

    public RestMainApp(MariaDB4jSpringService mariaDB4j) {
        this.mariaDB4j = mariaDB4j;
        try {
            mariaDB4j.getDB().source("schema.sql");
        } catch (ManagedProcessException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {

        SpringApplication app = new SpringApplication(RestMainApp.class);
        app.setBannerMode(Banner.Mode.OFF);
        ConfigurableApplicationContext ctx = app.run(args);

        MariaDB4jService.waitForKeyPressToCleanlyExit();

        ctx.stop();
        ctx.close();
    }

    @Override
    public int getExitCode() {
        return mariaDB4j.getLastException() == null ? 0 : -1;
    }
}