package com.bib.restcrudapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Book {
    private long id;
    private String isbn;
    private String name;
    private String genre;
    @JsonProperty("author_name")
    private String authorName;
    private Double price;

    public Book() {
    }

}
