package com.bib.restcrudapi.exception;

public class BookIdNotFoundException extends RuntimeException{
    public BookIdNotFoundException(String message)
    {
        super(message);
    }
}
