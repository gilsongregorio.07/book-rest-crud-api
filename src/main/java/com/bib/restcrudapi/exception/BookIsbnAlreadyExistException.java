package com.bib.restcrudapi.exception;

public class BookIsbnAlreadyExistException extends RuntimeException {
    public BookIsbnAlreadyExistException(String message) {
        super(message);
    }
}
