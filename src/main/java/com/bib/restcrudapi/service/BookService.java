package com.bib.restcrudapi.service;

import com.bib.restcrudapi.model.Book;

import java.util.List;
import java.util.Map;

public interface BookService {
    Book insertBook(Book book);

    Book updateBook(Book book);

    Map<String, Boolean> deleteBook(Long id);

    List<Book> getBooks();

    Book findById(Long id);
}
