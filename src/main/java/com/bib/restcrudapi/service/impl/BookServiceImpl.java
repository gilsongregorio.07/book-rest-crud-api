package com.bib.restcrudapi.service.impl;

import com.bib.restcrudapi.exception.BookIdNotFoundException;
import com.bib.restcrudapi.exception.BookIsbnAlreadyExistException;
import com.bib.restcrudapi.model.Book;
import com.bib.restcrudapi.repository.BookRepository;
import com.bib.restcrudapi.service.BookService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BookServiceImpl implements BookService {
    @Resource
    private BookRepository bookRepository;

    @Override
    public Book insertBook(Book book) {
        if(bookRepository.findByIsbn(book.getIsbn(), book.getId())==null) {
            bookRepository.insert(book);
            return book;
        }else
        {
            String message = "Book ISBN ["+book.getIsbn()+"] already exist";
            throw new BookIsbnAlreadyExistException(message);
        }
    }

    @Override
    public Book updateBook(Book book) {
        if(bookRepository.findByIsbn(book.getIsbn(), book.getId())==null){
            if(bookRepository.update(book)==0)
            {
                String message = "Book ID ["+book.getId()+"] not found";
                throw new BookIdNotFoundException(message);
            }
        }
        else {
            String message = "Book ISBN ["+book.getIsbn()+"] already exist";
            throw new BookIsbnAlreadyExistException(message);
        }

        return book;
    }

    @Override
    public Map<String, Boolean> deleteBook(Long id) {
        if(bookRepository.deleteById(id) != 0){
            Map<String, Boolean> response = new HashMap<>();
            response.put("deleted", Boolean.TRUE);
            return response;
        }else{
            String message = "Book ID ["+id+"] not found";
            throw new BookIdNotFoundException(message);
        }
    }

    @Override
    public List<Book> getBooks() {
        List<Book> books = bookRepository.findAll();
        return books;
    }

    @Override
    public Book findById(Long id) {
        return bookRepository.findById(id);
    }
}
