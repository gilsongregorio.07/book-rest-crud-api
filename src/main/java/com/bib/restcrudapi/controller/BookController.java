package com.bib.restcrudapi.controller;

import com.bib.restcrudapi.model.Book;
import com.bib.restcrudapi.service.BookService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("book/")
public class BookController {

    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }
    // get all books
    @GetMapping("/list")
    public List<Book> getAllBooks()
    {
        return bookService.getBooks();
    }

    // create book rest API
    @PostMapping("/new")
    public Book createBook(@RequestBody Book book)  {
        return bookService.insertBook(book);
    }

    // delete book rest api
    @PostMapping("/delete/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteBook(@PathVariable Long id) {

        return ResponseEntity.ok(bookService.deleteBook(id));
    }

    // update book rest api
    @PostMapping("/update/{id}")
    public ResponseEntity<Book> updateUser(@PathVariable Long id,
                                           @RequestBody Book bookDetails) {
        bookDetails.setId(id);



        return ResponseEntity.ok(bookService.updateBook(bookDetails));
    }
}

